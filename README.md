# Energy Forecasting #

### Monthly Forecasting ###

Baseline model using ARIMA - Ivan 

LSTM (potentially) for 12 months of data - Rita

LSTM (potentially) for 4 weeks of data - Wendy

### Daily Forecasting ###

Aggregation: Daily - 7 days with 1-2 day prediction

Baseline = T+1

ARIMA/ARIMAX - Dylan

Prophet/ FF Neural Net - Dhruv

Product Owner: Luca Stamatescu